var express = require("express");
var mysql = require("mysql");
var sql = require("mssql");
var session = require("express-session");
var router = express.Router();
var fs = require("fs");
const bcrypt = require("bcrypt");
const saltRounds = 10;
const myPlaintextPassword = "s0//P4$$w0rD";
const someOtherPlaintextPassword = "not_bacon";

var configSQL = {
  user: "sa",
  password: "12",
  server: "DESKTOP-HEL0HAI",
  database: "HR",
};
var configMysql = {
  host: "localhost",
  user: "root",
  password: "",
  database: "payroll",
};

router.use(
  session({
    secret: "keyboard cat",
    resave: false,
    saveUninitialized: true,
  })
);

router.get("/dashboard", (req, res) => {
  console.log(req.session.username);
  (async () => {
    try {
      let pool = await sql.connect(configSQL);
      let result = await pool
        .request()
        .query(
          "SELECT * FROM Personal as p , Job_History as h , Employment as e , Emergency_Contacts as ec  WHERE p.Employee_ID = h.Employee_ID and p.Employee_ID = e.Employee_ID and p.Employee_ID = ec.Employee_ID and e.Employee_ID = h.Employee_ID and ec.Phone_Number = p.Phone_Number"
        );
      console.log("DB1 result:");

      var pool2 = await mysql.createConnection(configMysql);
      var mysqlQuery =
        " SELECT * FROM employee as e  , pay_rates as p  WHERE  e.PayRates_id = p.idPay_Rates  ";

      pool2.query(mysqlQuery, (err, result2) => {
        if (err) throw err;
        console.log("DB2 result:");
        var data = {
          sqlDB1: result.recordset,
          mysqlDB2: result2,
        };

        // res.send(data);
        //console.log(result2);

        res.render("pages/dashboardE", {
          dashboardE: result2,
          dashboardH: result.recordset,
        });
      });
    } catch (err) {
      if (err) console.log(err);
    }
  })();
});

router.get("/login", function (req, res) {
  console.log(username);
  var { username } = req.session;
  if (req.session.username) {
    res.redirect("index");
  } else
    res.render("pages/login", {
      username: req.session.username,
    });
});
router.post("/logout", function (req, res) {
  req.session.username = undefined;
  res.redirect("/login");
});
router.get("/register", function (req, res) {
  var username = req.session.username;
  if (username) {
    res.redirect("login");
  } else res.render("pages/register");
});
router.post("/goregister", function (req, res) {
  const { email } = req.body;
  const { username } = req.body;
  const { password } = req.body;
  let pool2 = mysql.createConnection(configMysql);

   
    bcrypt.hash(password, saltRounds, function(err, hash) {
      pool2.connect(function (err) {
        if (err) throw err;
        pool2.query(
          "INSERT INTO users (User_Name , Password , Email  ) VALUES (?,?,?) ",
          [username, hash , email ],
          function (err, result) {
            if (err) throw err;
            console.log("thanhcong");
            res.redirect("login")
          }
        );
        
      });
    });
 
 

});
router.post("/gologin", (req, res, next) => {
  const { username } = req.body;
  const { password } = req.body;
  var pool2 = mysql.createConnection(configMysql);
  let pool = sql.connect(configSQL);
  if (username && password) {
    pool2.connect(function (err) {
      if (err) throw err;
      pool2.query(
        "SELECT * FROM users WHERE User_Name = ? ",
        [username],
        function (err, result) {
          if (err) throw err;
          if (result != 0) {
            bcrypt.compare(password, result[0].Password, function(err, result2) {
              if(result2 != 0){
                req.session.username = username
                res.redirect("index")
              }
          });          
          }
        }
      );
    });
  } else console.log("nhaptkmk");
});
router.get("/update", function (req, res) {
  if (req.session.username) {
    res.render("pages/update");
  } else res.redirect("login");
});
var executeQuery = function (res, parameters) {
  var request = new sql.Request();
  parameters.forEach(function (p) {
    request.input(p.name, p.type, p.value);
  });
};
router.post("/capnhap", function (req, res) {
  const { pay } = req.body;
  const { ID } = req.body;
  const { firt_name } = req.body;
  const { last_name } = req.body;
  const { phone } = req.body;
  const { gioitinh } = req.body;
  let pool2 = mysql.createConnection(configMysql);
  let pool = sql.connect(configSQL);
  pool2.connect(function (err) {
    if (err) throw err;
    pool2.query(
      "INSERT INTO pay_rates (Pay_Amount , idPay_Rates , Pay_Rate_Name ,Value ,Tax_Percentage ,Pay_Type ,PT_Level_C ) VALUES (?,?,'Staff','1000','9','1','2') ",
      [pay, ID],
      function (err, result) {
        if (err) throw err;
        console.log("thanhcong");
      }
    );
    pool2.query(
      "INSERT INTO employee (Employee_Number , idEmployee , Last_Name ,First_Name ,PayRates_id ,SSN ,Pay_Rate,Vacation_Days,Paid_To_Date,Paid_Last_Year) VALUES (?,?,?,?,? ,'10','10','10','10','10') ",
      [ID, ID, last_name, firt_name, ID],
      function (err, result) {
        if (err) throw err;
        console.log("thanhcong");
      }
    );
  });

  (async () => {
    try {
      let pool = await sql.connect(configSQL);
      let result = await pool
        .request()
        .query(
          "INSERT INTO Personal (Employee_ID,First_Name,Last_Name,Phone_Number,Gender,Shareholder_Status)   VALUES ('" +
            ID +
            "','" +
            firt_name +
            "','" +
            last_name +
            "','" +
            phone +
            "','" +
            gioitinh +
            "', 'true')"
        );
      let result2 = await pool
        .request()
        .query(
          "INSERT INTO Job_History (Employee_ID,Division)   VALUES ('" +
            ID +
            "', 'true')"
        );
      let result3 = await pool
        .request()
        .query(
          "INSERT INTO Employment (Employee_ID,Employment_Status)   VALUES ('" +
            ID +
            "', 'bad')"
        );
      let result4 = await pool
        .request()
        .query(
          "INSERT INTO Benefit_Plans (Plan_Name,Percentage_CoPay)   VALUES ('" +
            ID +
            "', 123)"
        );
      let result5 = await pool
        .request()
        .query(
          "INSERT INTO Emergency_Contacts (Employee_ID,Phone_Number)   VALUES ('" +
            ID +
            "', '" +
            phone +
            "')"
        );
      console.log("thành công");
    } catch (err) {
      if (err) console.log(err);
    }
  })();
});
//, Last_Name ,Middle_Initial ,Address1 ,Address2 ,City,State,Zip,Email,Phone_Number,Social_Security_Number,Drivers_License,Marital_Status,Gender,Shareholder_Status,Benefit_Plans,Ethnicity
//,?,'LEDUCDUNG','123','123','123','123','123','123@GMAIL.COM',?,'123','123','12',?,'true','NULL','true'    ,phone,gioitinh
// about page
router.get("/about", function (req, res) {
  res.render("pages/about");
});

router.get("/index", function (req, res) {
  (async () => {
    try {
      let pool = await sql.connect(configSQL);
      let result = await pool
        .request()
        .query(
          "SELECT * FROM Personal as p , Job_History as h , Employment as e , Emergency_Contacts as ec  WHERE p.Employee_ID = h.Employee_ID and p.Employee_ID = e.Employee_ID and p.Employee_ID = ec.Employee_ID and e.Employee_ID = h.Employee_ID and ec.Phone_Number = p.Phone_Number"
        );
      console.log("DB1 result:");

      var pool2 = await mysql.createConnection(configMysql);
      var mysqlQuery =
        " SELECT * FROM employee as e  , pay_rates as p  WHERE  e.PayRates_id = p.idPay_Rates  ";

      pool2.query(mysqlQuery, (err, result2) => {
        if (err) throw err;
        console.log("DB2 result:");
        var data = {
          sqlDB1: result.recordset,
          mysqlDB2: result2,
        };

        // res.send(data);
        //console.log(result2);
        res.render("pages/index2", {
          dashboardE: result2,
          dashboardH: result.recordset,
          username: req.session.username,
        });
      });
    } catch (err) {
      if (err) console.log(err);
    }
  })();
});
router.get("/error-404", function (req, res) {
  res.render("pages/error-404");
});
router.get("/form-basic", function (req, res) {
  var username = req.session.username;
  res.render("pages/form-basic", {
    username: username,
  });
});
router.get("/icon-material", function (req, res) {
  var username = req.session.username;
  res.render("pages/icon-material", {
    username,
  });
});
router.get("/pages-profile", function (req, res) {
  console.log(req.session.username);

  (async () => {
    var pool2 = await mysql.createConnection(configMysql);
    var mysqlQuery = " SELECT * FROM users  ";
    pool2.query(mysqlQuery, (err, result2) => {
      var data = {
        mysqlDB2: result2,
      };

      res.render("pages/pages-profile", {
        dashboardE: result2,
        username: req.session.username,
      });
      console.log(result2);
    });
  })();
});
router.get("/starter-kit", function (req, res) {
  var username = req.session.username;
  res.render("pages/starter-kit", {
    username: username,
  });
});
router.get("/table-basic", function (req, res) {
  var username = req.session.username;
  (async () => {
    try {
      let pool = await sql.connect(configSQL);
      let result = await pool
        .request()
        .query(
          "SELECT * FROM Personal as p , Job_History as h , Employment as e , Emergency_Contacts as ec  WHERE p.Employee_ID = h.Employee_ID and p.Employee_ID = e.Employee_ID and p.Employee_ID = ec.Employee_ID and e.Employee_ID = h.Employee_ID and ec.Phone_Number = p.Phone_Number"
        );
      let result3 = await pool.request().query("SELECT * FROM Benefit_Plans  ");

      var pool2 = await mysql.createConnection(configMysql);
      var mysqlQuery =
        " SELECT * FROM employee as e  , pay_rates as p  WHERE  e.PayRates_id = p.idPay_Rates  ";

      pool2.query(mysqlQuery, (err, result2) => {
        if (err) throw err;
        console.log("DB2 result:");
        var data = {
          sqlDB1: result.recordset,
          mysqlDB2: result2,
        };
        // res.send(data);
        //console.log(result2);
        res.render("pages/table-basic", {
          dashboardE: result2,
          dashboardH: result.recordset,
          dashboardBP: result3.recordset,
          username: username,
        });
      });
    } catch (err) {
      if (err) console.log(err);
    }
  })();
});
module.exports = router;
